#Snapdragon

[![Build Status](https://travis-ci.org/silvn/snapdragon.png)](https://travis-ci.org/silvn/snapdragon)

Biological analytics made fast and easy.

##Installation

	git submodule update --init
	./configure
	make
    make check
	make install